const util = require('util')
const asyncHandler = require('express-async-handler')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const users = []

// @desc Get test
// @route GET /api/test/
// @access PRIVATE
const getSampleTest = asyncHandler( async (request, response) => {
    response.status(200).json({message: 'Test sample'})
})

// @desc Post test
// @route POST /api/test/
// @access PRIVATE
const postSampleTest = asyncHandler( async (request, response)  => {
    console.log(request.body)
    if(Object.keys(request.body).length == 0){
        response.status(400)
        throw new Error('GIVE ME SOMETHING!')
    }    
    response.status(200).json({message: `Data received ${JSON.stringify(request.body)}`})
})

// @desc Delete goal
// @route DELETE /api/test/:testdata
// @access PRIVATE

const putSampleTest = asyncHandler(async (request, response) => {
    console.log(`Received ${request.body}`)
    response.status(200).json({message: `Data updated ${request.params.testdata} `})
})
// @desc Delete goal
// @route DELETE /api/test/:testdata
// @access PRIVATE

const deleteSampleTest = asyncHandler( async (request, response) => {
    response.status(200).json({message: `Data deleted ${request.params.testdata}`})
})

// @desc Register user
// @route POST /api/test/users
// @access PRIVATE

const createUsers = asyncHandler( async (request, response) => {
    const salt = await bcrypt.genSalt()
    const hashedPWD = await bcrypt.hash(request.body.password, salt) //can be done without gensalt in on method
    console.log(salt)
    console.log(hashedPWD)
    const user = {username: request.body.username, password: hashedPWD}
    users.push(user)

    if(!request.body.password){
        response.status(400)
        throw new Error('Invalid user info: No password field')
    }
    console.log(users)
    response.status(201).json({
        __username: user.username,
        __password: user.password,
        token: generateJWT(user.username)
    })
})

const loginUser = asyncHandler( async ( request, response) => {
    console.log("logging user in")
    const user = users.find(user => user.username = request.body.username )
    if(user==null){
        return response.status(400).send("Cannot find that user")
    }
    if(await bcrypt.compare(request.body.password, user.password)){
        response.json({
            __username: user.username,
            __password: user.password,
            token: generateJWT(user.username)
        } )
    } else {
        response.send("Unauthorized")
    }
})


const generateJWT = (username) => {
    return jwt.sign({username}, process.env.JWT_SECRET, {
        expiresIn: '3d',
    })
}

module.exports = {
    getSampleTest,
    postSampleTest,
    putSampleTest,
    deleteSampleTest,
    createUsers,
    loginUser,
    users
}
