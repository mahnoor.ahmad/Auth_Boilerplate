const jwt = require('jsonwebtoken')
const asyncHandler = require('express-async-handler')
const User = require('../controllers/testController')

const userstore = User.users
const protect = asyncHandler(async(request, response, next) => {
    let token

    if(request.headers.authorization && request.headers.authorization.startsWith('Bearer')){
        try {
            token = request.headers.authorization.split(' ')[1]
            console.log(token)
            const decoded = jwt.verify(token, process.env.JWT_SECRET)
            console.log(decoded)
            //Find username in token
            request.username = await userstore.find(user => request.username = decoded.username).username

            next()
        } catch (error){
            console.log(error)
            response.status(401)
            throw new Error('Not authorized: Invalid token')
        }
    }
    if (!token){
        response.status(401)
        throw new Error('Unauthorized: No token or invalid token')
    }
})

module.exports = protect