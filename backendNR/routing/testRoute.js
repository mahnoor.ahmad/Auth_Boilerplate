const express = require('express')
const routeit = express.Router()
const {
    getSampleTest, 
    postSampleTest, 
    putSampleTest, 
    deleteSampleTest, 
    createUsers, loginUser} = require('../controllers/testController')
const protect = require('../middleware/authenticateMiddleware')

routeit.route('/').get(protect, getSampleTest).post(protect, postSampleTest)

routeit.route('/:testdata').put(putSampleTest).delete(deleteSampleTest)

routeit.route('/users').post(createUsers)

routeit.post('/login', protect, loginUser)

module.exports = routeit