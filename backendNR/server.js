const express = require('express')
const dotenv = require('dotenv').config()
const {errorHandler} = require('./middleware/errMiddleware')
const serveport = process.env.PORT

const application = express()

application.use(express.json())
application.use(express.urlencoded({extended: true}))

application.use('/api/test', require('./routing/testRoute'))

application.use(errorHandler)
application.listen(serveport, () => console.log(`Listening on port ${serveport}`))
