import { useState, useEffect } from 'react'


function Login(_props) {
    const [formData, setFormData] = useState({
        email: '',
        password: '',
      })
    
      const { email, password } = formData
    return (
    <div className="login">
        <form> 

            <label htmlFor="username">Username</label>
            <input type="text" placeholder="Email or Phone" id="username" />
            <br></br>
            <label htmlFor="password">Password</label>
            <input type="password" placeholder="Password" id="password" />
            <div>
                <button className="loginb">Log In</button>
                <h3> Welcome! </h3>
            </div>
        </form>
    </div>
    );
}

export default Login;